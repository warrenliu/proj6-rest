# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## Authors
* Init by M Young
* Revised by Yiran Liu
* yiranl@uoregon.edu

## Use
```
docker-compose up --build
```
Must include --build flag, I don't know why
